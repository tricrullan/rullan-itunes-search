//
// This is a generated file, do not edit!
// Generated by R.swift, see https://github.com/mac-cain13/R.swift
//

import Foundation
import Rswift
import UIKit

/// This `R` struct is generated and contains references to static resources.
struct R: Rswift.Validatable {
  fileprivate static let applicationLocale = hostingBundle.preferredLocalizations.first.flatMap(Locale.init) ?? Locale.current
  fileprivate static let hostingBundle = Bundle(for: R.Class.self)
  
  static func validate() throws {
    try intern.validate()
  }
  
  /// This `R.image` struct is generated, and contains static references to 2 images.
  struct image {
    /// Image `icon_arrow`.
    static let icon_arrow = Rswift.ImageResource(bundle: R.hostingBundle, name: "icon_arrow")
    /// Image `image_placeholder`.
    static let image_placeholder = Rswift.ImageResource(bundle: R.hostingBundle, name: "image_placeholder")
    
    /// `UIImage(named: "icon_arrow", bundle: ..., traitCollection: ...)`
    static func icon_arrow(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.icon_arrow, compatibleWith: traitCollection)
    }
    
    /// `UIImage(named: "image_placeholder", bundle: ..., traitCollection: ...)`
    static func image_placeholder(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.image_placeholder, compatibleWith: traitCollection)
    }
    
    fileprivate init() {}
  }
  
  /// This `R.nib` struct is generated, and contains static references to 2 nibs.
  struct nib {
    /// Nib `TrackDetailsTableViewCell`.
    static let trackDetailsTableViewCell = _R.nib._TrackDetailsTableViewCell()
    /// Nib `TrackTableViewCell`.
    static let trackTableViewCell = _R.nib._TrackTableViewCell()
    
    /// `UINib(name: "TrackDetailsTableViewCell", in: bundle)`
    @available(*, deprecated, message: "Use UINib(resource: R.nib.trackDetailsTableViewCell) instead")
    static func trackDetailsTableViewCell(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.trackDetailsTableViewCell)
    }
    
    /// `UINib(name: "TrackTableViewCell", in: bundle)`
    @available(*, deprecated, message: "Use UINib(resource: R.nib.trackTableViewCell) instead")
    static func trackTableViewCell(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.trackTableViewCell)
    }
    
    static func trackDetailsTableViewCell(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TrackDetailsTableViewCell? {
      return R.nib.trackDetailsTableViewCell.instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TrackDetailsTableViewCell
    }
    
    static func trackTableViewCell(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TrackTableViewCell? {
      return R.nib.trackTableViewCell.instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TrackTableViewCell
    }
    
    fileprivate init() {}
  }
  
  /// This `R.reuseIdentifier` struct is generated, and contains static references to 2 reuse identifiers.
  struct reuseIdentifier {
    /// Reuse identifier `TrackDetailsTableViewCell`.
    static let trackDetailsTableViewCell: Rswift.ReuseIdentifier<TrackDetailsTableViewCell> = Rswift.ReuseIdentifier(identifier: "TrackDetailsTableViewCell")
    /// Reuse identifier `TrackTableViewCell`.
    static let trackTableViewCell: Rswift.ReuseIdentifier<TrackTableViewCell> = Rswift.ReuseIdentifier(identifier: "TrackTableViewCell")
    
    fileprivate init() {}
  }
  
  /// This `R.storyboard` struct is generated, and contains static references to 2 storyboards.
  struct storyboard {
    /// Storyboard `LaunchScreen`.
    static let launchScreen = _R.storyboard.launchScreen()
    /// Storyboard `Main`.
    static let main = _R.storyboard.main()
    
    /// `UIStoryboard(name: "LaunchScreen", bundle: ...)`
    static func launchScreen(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.launchScreen)
    }
    
    /// `UIStoryboard(name: "Main", bundle: ...)`
    static func main(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.main)
    }
    
    fileprivate init() {}
  }
  
  /// This `R.string` struct is generated, and contains static references to 1 localization tables.
  struct string {
    /// This `R.string.localizable` struct is generated, and contains static references to 4 localization keys.
    struct localizable {
      /// Value: Details
      static let details = Rswift.StringResource(key: "Details", tableName: "Localizable", bundle: R.hostingBundle, locales: [], comment: nil)
      /// Value: No Available Description
      static let noAvailableDescription = Rswift.StringResource(key: "No Available Description", tableName: "Localizable", bundle: R.hostingBundle, locales: [], comment: nil)
      /// Value: USD
      static let usD = Rswift.StringResource(key: "USD", tableName: "Localizable", bundle: R.hostingBundle, locales: [], comment: nil)
      /// Value: iTunes Search List
      static let iTunesSearchList = Rswift.StringResource(key: "iTunes Search List", tableName: "Localizable", bundle: R.hostingBundle, locales: [], comment: nil)
      
      /// Value: Details
      static func details(_: Void = ()) -> String {
        return NSLocalizedString("Details", bundle: R.hostingBundle, comment: "")
      }
      
      /// Value: No Available Description
      static func noAvailableDescription(_: Void = ()) -> String {
        return NSLocalizedString("No Available Description", bundle: R.hostingBundle, comment: "")
      }
      
      /// Value: USD
      static func usD(_: Void = ()) -> String {
        return NSLocalizedString("USD", bundle: R.hostingBundle, comment: "")
      }
      
      /// Value: iTunes Search List
      static func iTunesSearchList(_: Void = ()) -> String {
        return NSLocalizedString("iTunes Search List", bundle: R.hostingBundle, comment: "")
      }
      
      fileprivate init() {}
    }
    
    fileprivate init() {}
  }
  
  fileprivate struct intern: Rswift.Validatable {
    fileprivate static func validate() throws {
      try _R.validate()
    }
    
    fileprivate init() {}
  }
  
  fileprivate class Class {}
  
  fileprivate init() {}
}

struct _R: Rswift.Validatable {
  static func validate() throws {
    try storyboard.validate()
    try nib.validate()
  }
  
  struct nib: Rswift.Validatable {
    static func validate() throws {
      try _TrackTableViewCell.validate()
    }
    
    struct _TrackDetailsTableViewCell: Rswift.NibResourceType, Rswift.ReuseIdentifierType {
      typealias ReusableType = TrackDetailsTableViewCell
      
      let bundle = R.hostingBundle
      let identifier = "TrackDetailsTableViewCell"
      let name = "TrackDetailsTableViewCell"
      
      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TrackDetailsTableViewCell? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TrackDetailsTableViewCell
      }
      
      fileprivate init() {}
    }
    
    struct _TrackTableViewCell: Rswift.NibResourceType, Rswift.ReuseIdentifierType, Rswift.Validatable {
      typealias ReusableType = TrackTableViewCell
      
      let bundle = R.hostingBundle
      let identifier = "TrackTableViewCell"
      let name = "TrackTableViewCell"
      
      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TrackTableViewCell? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TrackTableViewCell
      }
      
      static func validate() throws {
        if UIKit.UIImage(named: "icon_arrow", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'icon_arrow' is used in nib 'TrackTableViewCell', but couldn't be loaded.") }
        if UIKit.UIImage(named: "image_placeholder", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'image_placeholder' is used in nib 'TrackTableViewCell', but couldn't be loaded.") }
        if #available(iOS 11.0, *) {
        }
      }
      
      fileprivate init() {}
    }
    
    fileprivate init() {}
  }
  
  struct storyboard: Rswift.Validatable {
    static func validate() throws {
      try launchScreen.validate()
      try main.validate()
    }
    
    struct launchScreen: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = UIKit.UIViewController
      
      let bundle = R.hostingBundle
      let name = "LaunchScreen"
      
      static func validate() throws {
        if #available(iOS 11.0, *) {
        }
      }
      
      fileprivate init() {}
    }
    
    struct main: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = UIKit.UISplitViewController
      
      let bundle = R.hostingBundle
      let name = "Main"
      let trackDetailViewController = StoryboardViewControllerResource<TrackDetailsViewController>(identifier: "TrackDetailViewController")
      
      func trackDetailViewController(_: Void = ()) -> TrackDetailsViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: trackDetailViewController)
      }
      
      static func validate() throws {
        if #available(iOS 11.0, *) {
        }
        if _R.storyboard.main().trackDetailViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'trackDetailViewController' could not be loaded from storyboard 'Main' as 'TrackDetailsViewController'.") }
      }
      
      fileprivate init() {}
    }
    
    fileprivate init() {}
  }
  
  fileprivate init() {}
}
