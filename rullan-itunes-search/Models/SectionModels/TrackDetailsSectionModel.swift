//
//  TrackDetailSectionModel.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 29/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit

struct TrackDetailsSectionModel {
   var items: [TrackDetailsItemModel]
}

enum TrackDetailsItemModel {
   case description(viewModel: TrackDetailsTableCellViewModel)
}

extension TrackDetailsItemModel {
   var reuseIdentifier: String {
      switch self {
      case .description:
         return R.nib.trackDetailsTableViewCell.identifier
      }
   }

   func viewModel<T>() -> T? {
      switch self {
      case let .description(viewModel):
         return viewModel as? T
      }
   }
}

protocol TrackDetailsItemModelBindableType {
   func setItemModel(_ itemModel: TrackDetailsItemModel)
}
