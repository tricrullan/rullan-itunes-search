//
//  TrackSectionModel.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit

struct TrackSectionModel {
   var items: [TrackItemModel]
}

enum TrackItemModel {
   case details(viewModel: TrackTableCellViewModel)
}

extension TrackItemModel {
   var reuseIdentifier: String {
      switch self {
      case .details:
         return R.nib.trackTableViewCell.identifier
      }
   }

   func viewModel<T>() -> T? {
      switch self {
      case let .details(viewModel):
         return viewModel as? T
      }
   }
}

protocol TrackItemModelBindableType {
   func setItemModel(_ itemModel: TrackItemModel)
}
