//
//  TrackTableCellViewModel.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit

struct TrackTableCellViewModel {

   init(track: Track) {
      self.trackName = track.trackName
      self.collectionName = track.collectionName
      self.imageURL = track.artworkURL
      self.genre = track.genre ?? ""
      self.priceTrack = track.priceTrack
      self.priceCollection = track.priceCollection
      self.currency = track.currency ?? R.string.localizable.usD()
   }

   let imageURL: String?
   let genre: String

   var name: String {
      var nameString = ""

      if let trackNameString = trackName {
         nameString = trackNameString
      } else if let collectionNameString = collectionName {
         nameString = collectionNameString
      }

      return nameString
   }

   var price: String {
      var priceString = ""

      if let priceTrack = priceTrack,
         priceTrack > 0.0 {
         priceString = "\(priceTrack)"
      } else if let priceCollection = priceCollection {
         priceString = "\(priceCollection)"
      }

      return "\(priceString) \(currency)"
   }

   private let priceCollection: Double?
   private let priceTrack: Double?
   private let currency: String
   private let trackName: String?
   private let collectionName: String?

}
