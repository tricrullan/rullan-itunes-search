//
//  TrackDetailsTableCellViewModel.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 29/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit

struct TrackDetailsTableCellViewModel {

   init(track: Track) {
      self.description = track.description ?? R.string.localizable.noAvailableDescription()
   }

   let description: String
}
