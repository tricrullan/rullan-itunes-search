//
//  Track.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import Foundation

struct TrackModel: Codable {
   let results: [Track]

   enum CodingKeys: String, CodingKey {
      case results
   }
}

// MARK: - Feature

struct Track: Codable {
   let trackName: String?
   let collectionName: String?
   let artworkURL: String?
   let priceTrack: Double?
   let priceCollection: Double?
   let currency: String?
   let genre: String?
   let description: String?

   enum CodingKeys: String, CodingKey {
      case trackName
      case collectionName
      case artworkURL = "artworkUrl100"
      case priceTrack = "trackPrice"
      case priceCollection = "collectionPrice"
      case currency
      case genre = "primaryGenreName"
      case description = "longDescription"
   }
}
