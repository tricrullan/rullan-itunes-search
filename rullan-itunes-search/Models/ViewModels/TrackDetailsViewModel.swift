//
//  TrackDetailsViewModel.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 29/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit

class TrackDetailsViewModel {

   init(trackModel: Track) {
      self.trackModel = trackModel
   }

   lazy var sectionModels: [TrackDetailsSectionModel] = {
      return makeSectionModels()
   }()

   var title: String {
      if let trackNameString = trackModel.trackName {
         return trackNameString
      } else if let collectionNameString = trackModel.collectionName {
         return collectionNameString
      }

      return  R.string.localizable.details()
   }


   // MARK: - Privates

   private var trackModel: Track

   private func makeSectionModels() -> [TrackDetailsSectionModel] {
      var sectionModels = [TrackDetailsSectionModel]()

      let model = TrackDetailsTableCellViewModel(track: trackModel)
      sectionModels.append(TrackDetailsSectionModel(items: [.description(viewModel: model)]))
      return sectionModels
   }

   // MARK: - Functions

   func itemModel(at indexPath: IndexPath) -> TrackDetailsItemModel {
      return sectionModels[indexPath.section].items[indexPath.row]
   }
}
