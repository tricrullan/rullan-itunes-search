//
//  TrackViewModel.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit
import CoreData

protocol TrackViewModelDelegate: class {
   func trackViewModel(_ viewModel: TrackViewModel, showDetailViewController trackModel: Track)
   func trackViewModelNeedsReloadData(_ viewModel: TrackViewModel)
}

class TrackViewModel {

   init(dataService: DataService = DataService(), coreDataManager: CoreDataManager = CoreDataManager.shared) {
      self.dataService = dataService
      self.coreDataManager = coreDataManager
      sectionModels = makeSectionModels()
   }

   var sectionModels = [TrackSectionModel]() {
      didSet {
         delegate?.trackViewModelNeedsReloadData(self)
      }
   }

   weak var delegate: TrackViewModelDelegate?

   // MARK: - Privates

   private var trackModel: TrackModel?
   private var dataService: DataService?
   private var coreDataManager: CoreDataManager

   private func makeSectionModels() -> [TrackSectionModel] {
      var sectionModels = [TrackSectionModel]()

      guard let model = trackModel else {
         return sectionModels
      }
      
      for index in 0..<model.results.count{
         let trackModel = model.results[index]
         let trackCellViewModel = TrackTableCellViewModel(track: trackModel)
         sectionModels.append(TrackSectionModel(items: [.details(viewModel: trackCellViewModel)]))
      }

      return sectionModels
   }

   private func saveTrackList() {
      if let trackList = trackModel?.results {
         coreDataManager.saveTrackList(trackList)
      }
   }

   private func loadSavedTrackList() {
      let trackList = coreDataManager.loadTrackList()

      guard let savedTrackList = trackList,
            savedTrackList.count > 0 else {
         getTrackListFromAPI()
         return
      }

      trackModel = TrackModel(results: savedTrackList)
      sectionModels = makeSectionModels()
   }

   private func getTrackListFromAPI() {
      dataService?.getTrack()
         .done { model -> Void in
            self.trackModel = model
            self.sectionModels = self.makeSectionModels()

            if let trackList = self.trackModel?.results {
               self.coreDataManager.saveTrackList(trackList)
            }

         }
         .catch { error in
            //Handle error or give feedback to the user
            print(error.localizedDescription)
      }



   }

   // MARK: - Functions

   func getTrackList() {
      loadSavedTrackList()
   }

   func itemModel(at indexPath: IndexPath) -> TrackItemModel {
      return sectionModels[indexPath.section].items[indexPath.row]
   }

   func onItemSelection(indexPath: IndexPath) {
      if let trackModel = trackModel {
         let selectedPath = indexPath.section
         let model = trackModel.results[selectedPath]
         delegate?.trackViewModel(self, showDetailViewController: model)
      }
   }
}

