//
//  Request.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

struct DataService {

   // MARK: - Singleton
   static let shared = DataService()

   // MARK: - URL
   private var trackURL = "https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all"

   // MARK: - Services

   func getTrack() -> Promise<TrackModel> {
      return Promise { seal in
         Alamofire.request(trackURL)
            .validate()
            .responseJSON { response in
               switch response.result {
               case .success:
                  guard let data = response.data else { return }
                  do {
                     let decoder = JSONDecoder()
                     let trackModel = try decoder.decode(TrackModel.self, from: data)
                     seal.fulfill(trackModel)
                  } catch let jsonError as NSError {
                     print("json error: \(jsonError.localizedDescription)")
                  }
               case .failure(let error):
                  seal.reject(error)
               }
         }
      }
   }

}
