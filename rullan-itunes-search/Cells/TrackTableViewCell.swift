//
//  TrackTableViewCell.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit
import AlamofireImage

class TrackTableViewCell: UITableViewCell {

   private struct Constants {
      static let imagePlaceholder = "image_placeholder"
      static let cornerRadius: CGFloat = 5
   }

   @IBOutlet weak private var photoImageView: UIImageView!
   @IBOutlet weak private var trackNameLabel: UILabel!
   @IBOutlet weak private var genreLabel: UILabel!
   @IBOutlet weak private var priceLabel: UILabel!


   var viewModel: TrackTableCellViewModel! {
      didSet {
         bindViewModel()
      }
   }

   override func awakeFromNib() {
      configureViews()
   }

   //MARK: - Private

   private func configureViews() {
      photoImageView.layer.cornerRadius = Constants.cornerRadius
      photoImageView.layer.masksToBounds = true
   }
   
   private func bindViewModel() {
      trackNameLabel.text = viewModel.name
      genreLabel.text = viewModel.genre
      priceLabel.text = viewModel.price

      if let urlString = viewModel.imageURL, let url = URL(string: urlString) {
         photoImageView.af_setImage(withURL: url,
                                    placeholderImage: UIImage.init(named: Constants.imagePlaceholder),
                                    imageTransition: .crossDissolve(0.2))
      }
   }
}

extension TrackTableViewCell: TrackItemModelBindableType {
   func setItemModel(_ itemModel: TrackItemModel) {
      guard let viewModel: TrackTableCellViewModel = itemModel.viewModel() else {
         fatalError("View model mismatched!")
      }

      self.viewModel = viewModel
   }
}
