//
//  TrackDetailsTableViewCell.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 29/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit

class TrackDetailsTableViewCell: UITableViewCell {

   @IBOutlet weak private var descriptionLabel: UILabel!

   var viewModel: TrackDetailsTableCellViewModel! {
      didSet {
         bindViewModel()
      }
   }

   //MARK: - Privates

   private func bindViewModel() {
      descriptionLabel.text = viewModel.description
   }
}

extension TrackDetailsTableViewCell: TrackDetailsItemModelBindableType {
   func setItemModel(_ itemModel: TrackDetailsItemModel) {
      guard let viewModel: TrackDetailsTableCellViewModel = itemModel.viewModel() else {
         fatalError("View model mismatched!")
      }
      
      self.viewModel = viewModel
   }
}
