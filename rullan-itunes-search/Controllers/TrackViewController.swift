//
//  TrackViewController.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit

class TrackViewController: UIViewController {

   private struct Constants {
      static let trackCellRowHeight: CGFloat = 110
   }

   @IBOutlet weak private var tableView: UITableView!

    var viewModel: TrackViewModel!

   override func viewDidLoad() {
      super.viewDidLoad()

      configureTableView()
      setupViewModel()
   }

   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)

      configureNavigationBar()
   }

   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(true)

      // needed to clear the text in the back navigation:
      self.navigationItem.title = ""
   }

   //MARK: - Privates

   private func setupViewModel() {
      viewModel = TrackViewModel()
      viewModel.delegate = self
      viewModel.getTrackList()
   }

   private func configureNavigationBar() {
      navigationController?.navigationBar.prefersLargeTitles = true
      navigationItem.title = R.string.localizable.iTunesSearchList()
   }

   private func configureTableView() {
      tableView.register(R.nib.trackTableViewCell)
   }
}

// MARK: - UITableViewDataSource

extension TrackViewController: UITableViewDataSource {

   func numberOfSections(in tableView: UITableView) -> Int {
      return viewModel.sectionModels.count
   }

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return viewModel.sectionModels[section].items.count
   }

   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let itemModel = viewModel.itemModel(at: indexPath)

      let cell = tableView.dequeueReusableCell(withIdentifier: itemModel.reuseIdentifier, for: indexPath)
      if let cell = cell as? TrackItemModelBindableType {
         cell.setItemModel(itemModel)
      }

      return cell
   }
}

// MARK: - UITableViewDelegate

extension TrackViewController: UITableViewDelegate {

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: true)
      viewModel.onItemSelection(indexPath: indexPath)
   }

   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      return .leastNormalMagnitude
   }

   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return Constants.trackCellRowHeight
   }

   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      return .leastNormalMagnitude
   }
}

// MARK: - TrackViewModelDelegate

extension TrackViewController: TrackViewModelDelegate {
   func trackViewModel(_ viewModel: TrackViewModel, showDetailViewController trackModel: Track) {

      guard let viewController = R.storyboard.main.trackDetailViewController() else {
         return
      }
      let model = TrackDetailsViewModel(trackModel: trackModel)
      viewController.viewModel = model
      navigationController?.pushViewController(viewController, animated: true)
   }

   func trackViewModelNeedsReloadData(_ viewModel: TrackViewModel) {
      tableView.reloadData()

      DispatchQueue.main.async { [weak self] in
         self?.tableView.reloadData()
      }
   }
}
