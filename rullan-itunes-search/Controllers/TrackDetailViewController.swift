//
//  TrackDetailsViewController.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 28/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import UIKit


class TrackDetailsViewController: UIViewController {

   @IBOutlet weak private var tableView: UITableView!

   var viewModel: TrackDetailsViewModel!

   override func viewDidLoad() {
      super.viewDidLoad()

      configureTableView()
      configureNavigationBar()
   }

   //MARK: - Privates

   func configureNavigationBar() {
      navigationController?.navigationBar.prefersLargeTitles = false
      navigationItem.title = viewModel.title
   }

   private func configureTableView() {
      tableView.register(R.nib.trackDetailsTableViewCell)
   }
}

// MARK: - UITableViewDataSource

extension TrackDetailsViewController: UITableViewDataSource {

   func numberOfSections(in tableView: UITableView) -> Int {
      return viewModel.sectionModels.count
   }

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return viewModel.sectionModels[section].items.count
   }

   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let itemModel = viewModel.itemModel(at: indexPath)

      let cell = tableView.dequeueReusableCell(withIdentifier: itemModel.reuseIdentifier, for: indexPath)
      if let cell = cell as? TrackDetailsItemModelBindableType {
         cell.setItemModel(itemModel)
      }

      return cell
   }
}

// MARK: - UITableViewDelegate

extension TrackDetailsViewController: UITableViewDelegate {

   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      return .leastNormalMagnitude
   }

   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableView.automaticDimension
   }

   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      return .leastNormalMagnitude
   }
}
