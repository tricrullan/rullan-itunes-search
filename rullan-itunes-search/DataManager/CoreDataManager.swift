//
//  CoreDataManager.swift
//  rullan-itunes-search
//
//  Created by Tricia Rullan on 29/08/2019.
//  Copyright © 2019 Tric Rullan. All rights reserved.
//

import CoreData
import Foundation

class CoreDataManager {

   private struct Constants {
      static let iTunesSearch = "iTunesSearch"
   }

   static let shared = CoreDataManager()

   init() {}

   lazy var persistentContainer: NSPersistentContainer = {
      let container = NSPersistentContainer(name: Constants.iTunesSearch)
      container.loadPersistentStores(completionHandler: { _, error in
         _ = error.map { fatalError("Unresolved error \($0)") }
      })
      return container
   }()

   var mainContext: NSManagedObjectContext {
      return persistentContainer.viewContext
   }

   // MARK: - Functions

   func backgroundContext() -> NSManagedObjectContext {
      return persistentContainer.newBackgroundContext()
   }

   func loadTrackList() -> [Track]? {
      let mainContext = CoreDataManager.shared.mainContext
      let fetchRequest: NSFetchRequest<TrackEntity> = TrackEntity.fetchRequest()
      var trackList = [Track]()

      do {
         let results = try mainContext.fetch(fetchRequest)
         for trackEntity in results {
            let track = Track(trackName: trackEntity.trackName,
                              collectionName: trackEntity.collectionName,
                              artworkURL: trackEntity.artworkURL,
                              priceTrack: getPriceCollection(trackEntity.priceCollection),
                              priceCollection: getPriceCollection(trackEntity.priceCollection),
                              currency: trackEntity.currency,
                              genre: trackEntity.genre,
                              description: trackEntity.longDescription)
            trackList.append(track)
         }

         return trackList
      }

      catch {
         debugPrint(error)
      }

      return nil
   }

   func saveTrackList(_ trackList: [Track]) {
      let context = CoreDataManager.shared.backgroundContext()

       context.perform {
          for track in trackList {
            let entity = TrackEntity.entity()
            let trackEntity = TrackEntity(entity: entity, insertInto: context)
            trackEntity.trackName = track.trackName
            trackEntity.collectionName = track.collectionName

            if let priceCollection = track.priceCollection {
               trackEntity.priceCollection = "\(priceCollection)"
            }

            if let priceTrack = track.priceTrack {
               trackEntity.priceCollection = "\(priceTrack)"
            }

            trackEntity.currency = track.currency
            trackEntity.longDescription = track.description
            trackEntity.artworkURL = track.artworkURL
            trackEntity.genre = track.genre

            print(trackEntity)

            do {
               try context.save()
            } catch let error as NSError {
               print("Could not save. \(error), \(error.userInfo)")
            }
         }
      }
   }

   // MARK: - Privates

   private func getPriceCollection(_ price: String?) -> Double {
      if let priceCollection = price {
         return Double(priceCollection) ?? 0.0
      }

      return 0.0
   }
}
