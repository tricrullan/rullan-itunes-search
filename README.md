# rullan-itunes-search

rullan-itunes-search is a mobile app developed by Tric Rullan. The app enables to list down information from itunes search api.

### Information includes:

- Track name
- Artwork Image
- Genre
- Prices
- Description

## Table of Contents

- [Getting Started](#getting-started)
  * [Environment setup](#environment-setup)
  * [Project setup](#project-setup)
- [Architecture, Design Patterns and Other Documentation](#architecture-design-patterns-and-other-documentation)

## Getting Started

### Environment setup

-   macOS Version: High Sierra 10.14.x
-   Xcode Version: 11.0
-   Swift Version: 4.2

### Project setup

**1. Dependencies**

Install the following if not already done so:

- Cocoapods: Installation instructions can be [found here](https://cocoapods.org/)


## Architecture, Design Patterns and Other Documentation

The following concepts are used throughout the codebase:

-   MVVM: MVVM presents a better separation of code which is very easy to maintain (removing and adding of code). Also it makes testing easier since view models are separated. 
-  Alamofire [referemce is found here](https://github.com/Alamofire/Alamofire) Alamofire is used to fetch the iTunes Search API.
-  PromiseKit [reference is found here](https://github.com/mxcl/PromiseKit) PromiseKit makes code more readable and easy to maintain when simplifing asynchronous calls.
-  R.Swift [reference is found here](https://github.com/mac-cain13/R.swift) R.Swift catches compile errors since it makes it easier to link resources such as strings, identifiers and file names.
- CoreData: CoreData makes it easy to manage when saving and loading multiple view models, in this case about 50. Also, it's good to take advantage of data model instead of listing a lot objects manually. 
